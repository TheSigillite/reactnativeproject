import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, FlatList, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Navigation } from 'react-native-navigation';
import { openDatabase } from 'react-native-sqlite-storage';

function Item({fdate,rtemp,wind,winddir,condi,ico}){
    return(
        <View style={styles.item}>
            <View style={styles.tempInfo}>
                <View style={styles.tempcolumn}>
                    <Text style={styles.gtext}>At {fdate.substring(0,fdate.length - 3)}</Text>
                </View>
                <View style={styles.tempcolumn}>
                    <Text style={styles.gtext}>Temperature {rtemp}°C</Text>
                </View>
            </View>
            <View style={styles.windbox}>
                <View style={styles.windinfo}>
                    <Text style={styles.gtext} textBreakStrategy={'balanced'}>Wind {wind} m/s @ {winddir}</Text>
                </View>
                <View style={styles.statusinfo}>
                    <Text style={styles.gtext}>{condi}</Text>
                </View>
            </View>
            <View style={styles.wicon}>
                <Image source={{uri: "http://openweathermap.org/img/w/"+ico+".png"}}
                    style={{width: Dimensions.get('window').width*0.1, height: Dimensions.get('window').height*0.1, resizeMode: 'contain'}}/>
            </View>
        </View>
    )
}

export default class NextDaysWeather extends Component{
    constructor(props){
        super(props)
        this.forecastendpoint = this.props.forecastendpoint
        this.forecast;
        this.location;
        this.indatabase = [];
        console.log("new endpont: "+ this.forecastendpoint);
        console.log("calculatedfont="+Dimensions.get('window').height*0.016);
        this.database = openDatabase({name: 'prevLocs.db', createFromLocation: 1});
        this.getLocationsFromDB();
    }

    getForecast(){
        return fetch(this.forecastendpoint).then((response) => response.json()).then(
            (responseJson) => {
                this.forecast = responseJson
                this.location = {city: responseJson.city.name, country: responseJson.city.country}
                console.log(this.location)
                this.setState({loading: false})
                return responseJson
            }
        ).catch(err => console.log(err))
    }

    saveWeather(){
       this.database.transaction(tr=>{
           if(this.verifyifInDB()){
               alert("This location is already in database");
           }
           else{
               tr.executeSql("INSERT INTO previousLocations (city, country) VALUES ('"+this.location.city+"', '"+this.location.country+"');",[],(tr,result)=>{
                   console.log(result);
               },err=>{console.error(err)})
           }
       })
       this.getLocationsFromDB()
    }

    getLocationsFromDB(){
        this.database.transaction(tra => {
            tra.executeSql('SELECT city, country FROM previousLocations',[],(tra,results)=>{
                this.indatabase = [];
                for(let i = 0;i<results.rows.length;i++){
                    this.indatabase.push(results.rows.item(i))
                    
                }
                console.log(this.indatabase)
            })
        })
    }

    verifyifInDB(){
        for(let i = 0;i<this.indatabase.length;i++){
            if(this.indatabase[i].city==this.location.city && this.indatabase[i].country==this.location.country){
                return true
            }
        }
        return false
    }

    goBack(){
        Navigation.pop('MainStack')
    }

    componentWillMount(){
        this.setState({loading: true})
        this.forecastendpoint = this.props.forecastendpoint
        this.getForecast()
    }

    render(){
        if(this.state.loading){
            return(
                <View><Text>Loading..</Text></View>
            )
        }else{
            return(
                <View style={styles.container}>
                    <View style={styles.topbar}>
                        <View style={styles.iconb}>
                            <Icon.Button name='keyboard-backspace'
                            backgroundColor='#B3E82C'
                            size={Dimensions.get('window').height*0.05}
                            onPress={() => this.goBack()}>

                            </Icon.Button>
                        </View>
                        <View style={styles.toptextbox}>
                            <Text style={styles.toptext}>{this.forecast.city.name} , {this.forecast.city.country}</Text>
                        </View>
                        <View style={styles.iconb}>
                            <Icon.Button name='save'
                            backgroundColor="#B3E82C"
                            size={Dimensions.get('window').height*0.05}
                            onPress={() => this.saveWeather()}>

                            </Icon.Button>
                        </View>
                    </View>
                   <SafeAreaView style={styles.forecastlist}>
                    <FlatList data={this.forecast.list}
                        renderItem={({item}) => 
                        <Item fdate={item.dt_txt}
                        rtemp={item.main.temp}
                        wind={item.wind.speed}
                        winddir={item.wind.deg}
                        condi={item.weather[0].description}
                        ico={item.weather[0].icon}/>}/>
                    </SafeAreaView>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'stretch',
        alignSelf: 'stretch'
    },
    topbar: {
        flex: 1,
        justifyContent: 'space-evenly',
        alignSelf: 'stretch',
        flexDirection:'row'
    },
    forecastlist:{
        flex: 12,
        backgroundColor: '#F5E147',
        alignItems: 'stretch'
    },
    item:{
        flex: 4,
        flexDirection: 'row',
        backgroundColor: 'yellow',
        alignSelf: 'stretch',
        paddingTop: 2,
        paddingBottom: 2
    },
    wicon:{
        //flex: 3,
        alignSelf: 'stretch',
        backgroundColor: '#E8D338',
        borderWidth: 2,
        borderColor: 'black'
    },
    tempInfo:{
        flex: 5,
        alignSelf: 'stretch',
        backgroundColor: '#B5E838',
        borderWidth: 2,
        borderColor: 'black',
        flexDirection: 'column'
    },
    windbox:{
        flex: 5,
        alignSelf: 'stretch',
        backgroundColor: '#FFFC4A',
        borderWidth: 2,
        borderColor: 'black',
        flexDirection: 'column'
    },
    statusinfo:{
        flex: 2,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: 'black'
    },
    windinfo:{
        flex: 3,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: 'black'
    },
    tempcolumn:{
        flex: 1,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: 'black'
    },
    gtext:{
        fontSize: Dimensions.get('window').height*0.02,
        textAlign: 'center'
    },
    iconb:{
        flex: 1,
        alignSelf: 'stretch'
    },
    toptextbox:{
        flex: 4,
        alignSelf: 'stretch',
        justifyContent: 'center',
        backgroundColor: '#C3EB4E'
    },
    toptext:{
        fontSize: Dimensions.get('window').height*0.04,
        textAlign: 'center'
    }
})