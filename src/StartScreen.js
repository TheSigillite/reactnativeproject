import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Dimensions, PermissionsAndroid, Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Geolocation from 'react-native-geolocation-service';
import { Navigation } from 'react-native-navigation';
type Props = {};
export default class StartScreen extends Component<Props>{
    constructor(){
        super();
        this.gpsPermission;
        this.gpsWeather;
        this.lat;
        this.lon;
    }

    checkGPSPermAndGo(){
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((resolution) => {
            this.gpsPermission = resolution
            console.log(resolution)
            if(resolution==false){
                this.requestGPSPerm()
            }
            else{
                Geolocation.getCurrentPosition(
                    (position) => {
                        console.log(position);
                        //alert("lat "+position.coords.latitude+" lon "+position.coords.longitude)
                        //this.gpsWeather = this.getWeatherByCoords(position.coords.latitude,position.coords.longitude)
                        this.lon = position.coords.longitude
                        this.lat = position.coords.latitude
                        console.log("lat "+this.lat+" lon "+this.lon)
                        this.goToCurrentWeatherWithGPS('http://api.openweathermap.org/data/2.5/weather?lat='+this.lat+'&lon='+this.lon+'&appid=f0f86ac9c889cc551630a3bfd1e973e4&units=metric')
                    },
                    (error) => {
                        // See error code charts below.
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        })
    }

    async requestGPSPerm(){
        try{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Application needs to use GPS',
                    message: 'TrashWeather needs your permission to use your phone GPS',
                    buttonNeutral: 'Not Now',
                    buttonPositive: 'OK',
                    buttonNegative: 'Cancel'
                },
            );
            if(granted === PermissionsAndroid.RESULTS.GRANTED){
                this.gpsPermission = true;
                console.log('permission granted')
                return true
            }
            else{
                console.log('nope')
                return false
            }
        }catch(err){
            console.warn(err)
        }
    }

    goToCurrentWeatherWithGPS = (fetchURL) =>{
        Navigation.push('MainStack',{
            component: {
                name: 'CurrentWeather',
                passProps: {
                    endpoint: fetchURL
                }
            }
        })
    }

    goToLocationSelector = () =>{
        Navigation.push('MainStack',{
            component: {
                name: 'LocationSelector'
            }
        })
    }


    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={require('./res/AppIconSquare.png')} style={styles.image}/>
                </View>
                <View style={styles.buttonsContainer}>
                   <View style={styles.iconsSection}>
                        <View style={styles.singleIcon}>
                            <Icon.Button name="my-location" 
                                size={Dimensions.get('window').height * 0.25} 
                                backgroundColor = "#BAE84F"
                                borderRadius={10}
                                onPress={() => this.checkGPSPermAndGo()}> 
                                    <Text style={{fontSize: Dimensions.get('window').height*0.03}}>
                                     Locate With GPS
                                    </Text> 
                                </Icon.Button>
                        </View>
                        <View style={styles.singleIcon}>
                            <Icon.Button name="near-me" 
                                size={Dimensions.get('window').height * 0.25} 
                                backgroundColor = "#BAE84F"
                                borderRadius={10}
                                onPress={() => this.goToLocationSelector()}> 
                                    <Text style={{fontSize: Dimensions.get('window').height*0.03}}>
                                     Select Your Location
                                    </Text> 
                                </Icon.Button>
                        </View>
                   </View>
                </View>
            </View>
        )
    }

    
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            alignItems:"center"
        },
        logoContainer: {
            flex: 4,
            justifyContent: "center",
            backgroundColor: "orange",
            alignSelf: "stretch",
        },
        buttonsContainer: {
            flex: 5,
            justifyContent: "flex-start",
            alignItems: "flex-start",
            backgroundColor: "yellow",
            alignSelf: "stretch",
            flexDirection: "row"
        },
        iconsSection: {
            flex: 1,
            alignSelf: "stretch",
            justifyContent: "space-around",
            flexDirection: "column",
            backgroundColor: "red"
        },
        buttonsSection: {
            flex: 1,
            alignSelf: "stretch",
            justifyContent: "space-around",
            flexDirection: "row",
            backgroundColor: "green"
        },
        singleIcon:{
            flex: 1,
            alignSelf: "stretch",
            justifyContent: "center",
            alignItems: "stretch",
            borderRadius: 2,
            borderWidth: 0.5,
            borderColor: "black"
        },
        image: {
            flex: 1,
            aspectRatio: 1.3,
            resizeMode: 'contain',
          }
        
    });