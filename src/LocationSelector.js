import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Dimensions, Image, SafeAreaView, Modal, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Input } from 'react-native-elements';
import MapView from 'react-native-maps';
import { Navigation } from 'react-native-navigation';
import { openDatabase } from 'react-native-sqlite-storage';
import { CustomPicker } from 'react-native-custom-picker';

export default class LocationSelector extends Component{
    constructor(){
        super()
        this.state = {
            modalShowed: false,
            location: 'Tarnow',
            country: 'pl',
        }
        /*
        var database = openDatabase({name: 'prevLocs.db', createFromLocation: 1})
        this.indb = [];
        database.transaction(tx => {
            tx.executeSql('SELECT city, country FROM previousLocations',[],(tx,results)=>{
                var temp = []
                for(let i = 0;i<results.rows.length;i++){
                    temp.push(results.rows.item(i))
                    console.log(results.rows.item(i))
                }
                this.setState({
                    oldlocs: temp
                })
            })
           
        },err => {alert(err)})
        */
    }

    goToWeatherWithProps(){
        this.endpoint = 'http://api.openweathermap.org/data/2.5/weather?q='+this.state.location+','+this.state.country+'&appid=f0f86ac9c889cc551630a3bfd1e973e4&units=metric'
        Navigation.push('MainStack',{
            component: {
                name: 'CurrentWeather',
                passProps: {
                    endpoint: this.endpoint
                }
            }
        })
    }

    render(){
        const options = [
            {
                img: require('./res/at.png'),
                label: 'Austria',
                value: 'at'
            },
            {
                img: require('./res/be.png'),
                label: 'Belgium',
                value: 'be'
            },
            {
                img: require('./res/bg.png'),
                label: 'Bulgaria',
                value: 'bg'
            },
            {
                img: require('./res/gb.png'),
                label: 'Britan',
                value: 'gb'
            },
            {
                img: require('./res/hr.png'),
                label: 'Croatia',
                value: 'hr'
            },
            {
                img: require('./res/cy.png'),
                label: 'Cyprus',
                value: 'cy'
            },
            {
                img: require('./res/cz.png'),
                label: "Czechia",
                value: 'cz'
            },
            {
                img: require('./res/dk.png'),
                label: 'Denmark',
                value: 'dk'
            },
            {
                img: require('./res/fi.png'),
                label: 'Finland',
                value: 'fi'
            },
            {
                img: require('./res/fr.png'),
                label: 'France',
                value: 'fr'
            },
            {
                img: require('./res/hr.png'),
                label: 'Croatia',
                value: 'hr'
            },
            {
                img: require('./res/de.png'),
                label: 'Germany',
                value: 'de'
            },
            {
                img: require('./res/hu.png'),
                label: 'Hungary',
                value: 'hu'
            },
            {
                img: require('./res/is.png'),
                label: 'Iceland',
                value: 'is'
            },
            {
                img: require('./res/ie.png'),
                label: 'Ireland',
                value: 'ie'
            },
            {
                img: require('./res/it.png'),
                label: 'Italy',
                value: 'it'
            },
            {
                img: require('./res/lt.png'),
                label: 'Lithuania',
                value: 'lt'
            },
            {
                img: require('./res/nl.png'),
                label: 'The Netherlands',
                value: 'nl'
            },
            {
                img: require('./res/no.png'),
                label: 'Norway',
                value: 'no'
            },
            {
                img: require('./res/pl.png'),
                label: 'Poland',
                value: 'pl'
            },
            {
                img: require('./res/ro.png'),
                label: 'Romania',
                value: 'ro'
            },
            {
                img: require('./res/ru.png'),
                label: 'Russia',
                value: 'ru'
            },
            {
                img: require('./res/sk.png'),
                label: 'Slovakia',
                value: 'sk'
            },
            {
                img: require('./res/es.png'),
                label: 'Spain',
                value: 'es'
            },
            {
                img: require('./res/se.png'),
                label: 'Sweden',
                value: 'se'
            },
            {
                img: require('./res/ch.png'),
                label: 'Switzerland',
                value: 'ch'
            },
            {
                img: require('./res/ua.png'),
                label: 'Ukraine',
                value: 'ua'
            },
            
        ]
        return(
            <View style={styles.container}>
                <View style={styles.selectors}>
                    <View style={styles.cityintitle}>
                        <Text style={styles.citytext}>Input Your location</Text>
                        <Input placeholder='city name' onChangeText={text => this.setState({location: text})}/>
                    </View>
                    <View style={styles.countryititle}>
                        <Text style={styles.citytext}>Select Your country</Text>
                        <TouchableOpacity style={styles.opacity}
                            onPress={() => this.setState({modalShowed: true})}>
                            <Text style={styles.citytext}>Select</Text>
                        </TouchableOpacity>
                        <View style={styles.countryititle}>
                            <CustomPicker
                                placeholder={'Select Your Country'}
                                options={options}
                                getLabel={item => item.label}
                                fieldTemplate={this.renderField}
                                optionTemplate={this.renderOption}
                                headerTemplate={this.renderHeader}
                                onValueChange={value => {
                                    this.setState({country: value.value})
                                }}/>
                        </View>
                    </View>
                </View>
                <View style={styles.continue}>
                    <Icon.Button name="near-me" 
                        size={Dimensions.get('window').height * 0.25} 
                        backgroundColor = "#BAE84F"
                        borderRadius={10}
                        onPress={() => this.goToWeatherWithProps()}> 
                        <Text style={{fontSize: Dimensions.get('window').height*0.03}}>
                            Go to That Location
                        </Text> 
                </Icon.Button>
                </View>
            </View>
        )
    }

    renderOption(settings){
        const {item, getLabel} = settings
        return(
            <View style={styles.optionsContainer}>
                <View style={styles.innerContainer}>
                    <Image source={item.img} style={{
                        width: Dimensions.get('window').width*0.18,
                        height:Dimensions.get('window').height*0.18,
                        resizeMode: 'contain'}}/>
                    <Text style={styles.citytext}>{getLabel(item)}</Text>
                </View>
            </View>
        )
    }
    renderField(settings){
        const {selectedItem, defaultText, getLabel} = settings
        return(
            <View style={styles.pickerfield}>
                <View>
                    {!selectedItem && <Text style={styles.citytext}>{defaultText}</Text>}
                    {selectedItem && (
                    <View style={styles.innerContainer}>
                        <Text style={styles.citytext}>
                                {getLabel(selectedItem)}
                        </Text>
                    </View>
                    )}
                </View>
            </View>
        )
    }
    renderHeader(){
        return(
            <View style={styles.headercont}>
                <Text style={styles.headerText}>Select Country</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignSelf:'stretch',
        backgroundColor: '#BAE84F'
    },
    continue:{
        flex: 3,
        alignSelf: 'stretch'
    },
    selectors:{
        flex:7,
        alignSelf: 'stretch',
        backgroundColor: 'grey'
    },
    cityintitle:{
        flex: 2,
        alignSelf: 'stretch',
        backgroundColor: '#F5E147',
        justifyContent: 'center',
        alignItems: 'center'
    },
    countryititle:{
        flex: 3,
        alignSelf: 'stretch',
        backgroundColor: '#FFFC4A'
    },
    citytext:{
        fontSize: Dimensions.get('window').height*0.04,
        textAlign: 'center'
    },
    opacity:{
        flex: 1,
        alignSelf: 'center',
        margin: 20
    },
    optionsContainer:{
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'stretch',
        alignItems: 'stretch',
        borderWidth: 1,
        borderColor: 'grey',
        padding: 3
    },
    textcontainer:{
        flex: 3,
        alignSelf: 'stretch'
    },
    imagecontainer:{
        flex:1,
        alignSelf: 'stretch'
    },
    innerContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'stretch'
    },
    pickerfield:{
        borderWidth: 1,
        borderColor: 'grey',
        alignSelf: 'stretch'
    },
    headercont:{
        textAlign: 'center',
        padding: 5
    },
    headerText: {
        fontSize: Dimensions.get('window').height*0.03,
        textAlign: 'center'
    }
})