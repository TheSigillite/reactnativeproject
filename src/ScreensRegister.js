import { Navigation } from "react-native-navigation";

export function registerScreens() {
    Navigation.registerComponent('Start', () => require('./App').default)
    Navigation.registerComponent('StartScreen', () => require('./StartScreen').default)
    Navigation.registerComponent('PreviousLocations', () => require('./PreviousLocations').default)
    Navigation.registerComponent('CurrentWeather', () => require('./CurrentWeather').default)
    Navigation.registerComponent('NextDaysWeather', () => require('./NextDaysWeather').default)
    Navigation.registerComponent('LocationSelector',() => require('./LocationSelector').default)
}