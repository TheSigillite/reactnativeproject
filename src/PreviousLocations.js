import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, FlatList} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import { Navigation } from 'react-native-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default class PreviousLocations extends Component{
    constructor(){
        super();
        this.prevLocs = [];
        this.oldLocs = [];
        this.database;
        
    }

    getLocations(){
        this.database.transaction(tra => {
            tra.executeSql('SELECT city, country FROM previousLocations',[],(tra,results)=>{
                this.prevLocs = [];
                for(let i = 0;i<results.rows.length;i++){
                    this.prevLocs.push(results.rows.item(i))
                }
                console.log("sidebar: "+this.prevLocs)
                //this.setState({load: false})
            })
        })
        this.getHistoricLocations()
    }

    getHistoricLocations(){
        this.database.transaction(tra => {
            tra.executeSql('SELECT city, country FROM History',[],(tra,results)=>{
                this.oldLocs = [];
                for(let i = 0;i<results.rows.length;i++){
                    this.oldLocs.push(results.rows.item(i))
                }
                console.log("sidebar history: "+this.oldLocs)
                this.setState({load: false})
            })
        })
    }

    componentWillMount(){
        this.setState({load: true})
        this.database = openDatabase({name: 'prevLocs.db', createFromLocation: 1});
        this.getLocations()
    }

    goToWeather(ct,co){
        this.endpoint = 'http://api.openweathermap.org/data/2.5/weather?q='+ct+','+co+'&appid=f0f86ac9c889cc551630a3bfd1e973e4&units=metric';
        Navigation.push('MainStack',{
            component: {
                name: 'CurrentWeather',
                passProps: {
                    endpoint: this.endpoint
                }
            }
        })
    }

    deleteLoc(ct){
        this.database.transaction(tr =>{
            tr.executeSql("DELETE FROM previousLocations WHERE city='"+ct+"'",[],(tr,res)=>{
                alert('Localization: '+ct+' removed')
            },err=>{alert(err)})
        },err=>{alert(err)})
        this.getLocations()
    }

    verifyifInDB(locat){
        for(let i = 0;i<this.prevLocs.length;i++){
            if(this.prevLocs[i].city==locat.city && this.prevLocs[i].country==locat.country){
                return true
            }
        }
        return false
    }

    saveToDatabase(loc){
        this.database.transaction(tr=>{
            if(this.verifyifInDB(loc)){
                alert("This location is already in database");
            }
            else{
                tr.executeSql("INSERT INTO previousLocations (city, country) VALUES ('"+loc.city+"', '"+loc.country+"');",[],(tr,result)=>{
                    console.log("saved"+result);
                },err=>{console.error(err)})
            }
        })
        this.getLocations()
    }

    deleteHistory(){
        this.database.transaction(tra => {
            tra.executeSql('DELETE FROM History',[],(tra,results)=>{
                console.log('History cleared')
            })
        })
        this.getLocations()
    }

    generateLocations(item){
        return(
            <View style={styles.item}>
                <TouchableOpacity style={styles.textButton} onPress={() => this.goToWeather(item.city,item.country)}>
                    <Text style={styles.itemText}> {item.city},{item.country} </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.xButton} onPress={() => this.deleteLoc(item.city)}>
                    <Text style={styles.itemText}> X </Text>
                </TouchableOpacity>
            </View>
        )
    }

    generateHistoryItems(item){
        return(
            <View style={styles.item}>
            <TouchableOpacity style={styles.textButton} onPress={() => this.goToWeather(item.city,item.country)}>
                <Text style={styles.itemText}>{item.city},{item.country}</Text>
            </TouchableOpacity>
            <Icon.Button name='save'
                size={Dimensions.get('window').height*0.04}
                backgroundColor='#B3E82C'
                onPress={() => this.saveToDatabase(item)}></Icon.Button>
            </View>
        )
    }

    render(){
        if(this.state.load){
            return(
                <View><Text>Loading..</Text></View>
            )
        }else{
        return(
            <View style={{alignSelf: 'stretch', flex:1, }}>
            <View style={styles.container}>
                <View style={styles.topbar}>
                    <Icon.Button name='refresh'
                     size={Dimensions.get('window').height*0.05}
                     backgroundColor='#B3E82C'
                     onPress={() => this.getLocations()}></Icon.Button>
                    <Text style={styles.itemText}>Saved Locations:</Text>
                </View>
                <View style={styles.locationslist}>
                    <FlatList data={this.prevLocs}
                    renderItem={({item}) => this.generateLocations(item)}/>
                </View>
            </View>
            <View style = {styles.container}>
                <View style={styles.topbar}>
                    <Icon.Button name='refresh'
                     size={Dimensions.get('window').height*0.05}
                     backgroundColor='#B3E82C'
                     onPress={() => this.getLocations()}></Icon.Button>
                    <Text style={styles.secondaryText}>Previously Visited</Text>
                    <Icon.Button name='delete'
                     size={Dimensions.get('window').height*0.05}
                     backgroundColor='#B3E82C'
                     onPress={() => this.deleteHistory()}></Icon.Button>
                </View>
                <View style={styles.locationslist}>
                    <FlatList data={this.oldLocs}
                    renderItem={({item}) => this.generateHistoryItems(item)}/>
                </View>
            </View>
            </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#B3E82C',
        alignSelf: 'stretch'
    },
    topbar: {
        flex: 1.5,
        alignItems: 'flex-start',
        flexDirection:'row',
        borderRadius: 1,
        borderColor: 'black'
    },
    locationslist: {
        flex: 9,
        backgroundColor: '#F5E147',
        alignSelf: 'stretch',
        alignItems: 'stretch'
    },
    item:{
        flex: 4,
        flexDirection: 'row',
        alignSelf: 'stretch',
        paddingTop: 2,
        paddingBottom: 2
    },
    itemText:{
        fontSize: Dimensions.get('window').height*0.04,
        fontStyle: 'normal',
        textAlign: 'center'
    },
    secondaryText:{
        fontSize: Dimensions.get('window').height*0.035,
        fontStyle: 'normal',
        textAlign: 'center'
    },
    textButton:{
        flex:2,
        alignSelf:'stretch',
        borderWidth: 2,
        borderColor: 'black',
    },
    xButton:{
        flex:1,
        alignSelf:'stretch',
        borderWidth: 2,
        borderColor: 'black',
    }
})