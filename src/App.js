import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, TextInput} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

type Props = {};
export default class App extends Component<Props>{

  constructor() {
    super();
    this.lat = 50.011843;
    this.lon = 20.866016;
    this.state = {
      lati: 50.011843,
      longi: 20.866016
    }
  }

  handleLat = (text) =>{
    this.lat = parseFloat(text)
  }
  handleLon = (text) =>{
    this.lon = parseFloat(text)
  }

  updateMapPiece = () =>{
    this.setState({lati: this.lat, longi: this.lon})
  }


	render(){
		return(
			<View style={styles.container}>
     <MapView
       provider={PROVIDER_GOOGLE} // remove if not using Google Maps
       style={styles.map}
       region={{
         latitude: this.state.lati,
         longitude: this.state.longi,
         latitudeDelta: 0.01,
         longitudeDelta: 0.01,
       }}
     >
     </MapView>
     <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Lattitude"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.handleLat}/>
      <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "Longitude"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.handleLon}/>
               <TouchableOpacity
               style = {styles.submitButton}
               onPress = {
                  () => this.updateMapPiece()
               }>
               <Text style = {styles.submitButtonText}> Submit </Text>
            </TouchableOpacity>
   </View>
	)}
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 350
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
 },
 submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
 }
});