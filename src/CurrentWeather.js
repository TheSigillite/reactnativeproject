import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MapView from 'react-native-maps';
import { openDatabase } from 'react-native-sqlite-storage';
import { Navigation } from 'react-native-navigation';

export default class CurrentWeather extends Component{
    constructor(props){
        super(props)
        this.weatherendpoint = this.props.endpoint
        this.nowWeather;
        console.log(this.weatherendpoint);
        this.database = openDatabase({name: 'prevLocs.db', createFromLocation: 1});
    }

    getWeather(){
        return fetch(this.weatherendpoint).then((response) => response.json()).then(
            (responseJson) => {
                this.nowWeather = responseJson
                //this.saveHistory()
                this.setState({loading: false})
                return responseJson
            }
        ).catch(err => {console.log(err)})
    }

    componentWillMount(){
        this.setState({loading: true})
        this.weatherendpoint = this.props.endpoint
        this.getWeather()
    }

    goToForecast = () =>{
        this.forecastURL = 'http://api.openweathermap.org/data/2.5/forecast?lat='+this.nowWeather.coord.lat+'&lon='+this.nowWeather.coord.lon+'&appid=f0f86ac9c889cc551630a3bfd1e973e4&units=metric'
        Navigation.push('MainStack',{
            component: {
                name: 'NextDaysWeather',
                passProps: {
                    forecastendpoint: this.forecastURL
                }
            }
        })
    }

    saveHistory(){
        this.database.transaction(tr => {
            tr.executeSql("INSERT INTO History (city,country) VALUES ('"+this.nowWeather.name+"', '"+this.nowWeather.sys.country+"');",[],(tr,result)=>{
                console.log("Saving to History "+result);
            },err=>{console.warn(err)})
        })
    }
    

    render(){
        
        if(this.state.loading){
            return(
                <View>
                    
                </View>
            )
        }
        else{
            this.saveHistory();
            return(
                <View style={styles.container}>
                    <View style={styles.infoSection}>
                         <View style={styles.iconAndBasceInfo}>
                             <View style={styles.iconProper}>
                                <Image source={{uri: "http://openweathermap.org/img/w/"+this.nowWeather.weather[0].icon+".png"}}
                                        style={{width: Dimensions.get('window').width*0.4, height: Dimensions.get('window').height*0.3, resizeMode: 'contain'}}/>
                             </View>
                             <View style={styles.basicInfo}>
                                <View style={styles.textOutliner}>
                                    <Text style={styles.basicText}> {this.nowWeather.name},{this.nowWeather.sys.country}</Text>
                                </View>
                                <View style={styles.textOutliner}>
                                    <Text style={styles.basicText}>{this.nowWeather.weather[0].description}</Text>
                                </View>
                                <View style={styles.windOutliner}>
                                    <Text textBreakStrategy={'balanced'} style={styles.basicText}>Wind {this.nowWeather.wind.speed} m/s @ {this.nowWeather.wind.deg}°</Text>
                                </View>
                             </View>
                             <TouchableOpacity style={styles.to5Day}>
                                <Icon name='arrow-forward'
                                    color='white'
                                    size={Dimensions.get('window').height * 0.1}
                                    onPress={() => this.goToForecast()}>
                                </Icon>
                             </TouchableOpacity>
                         </View>
                         <View style={styles.tempInfo}>
                             <View style={styles.textOutliner}>
                                <Text style={styles.temperatureText} textBreakStrategy={'balanced'}>Temperature is {this.nowWeather.main.temp}°C now, but it might feel like {this.nowWeather.main.feels_like}</Text>
                             </View>
                             <View style={styles.textOutliner}>
                                <Text style={styles.temperatureText} textBreakStrategy={'balanced'}>The lowest it has been today was {this.nowWeather.main.temp_min} °C</Text>
                             </View>
                             <View style={styles.textOutliner}>
                                <Text style={styles.temperatureText} textBreakStrategy={'balanced'}>The the highest is/was {this.nowWeather.main.temp_max} °C</Text>
                             </View>
                         </View>
                    </View>
                    <View style={styles.mapsection}>
                    <MapView initialRegion={{
                            latitude: this.nowWeather.coord.lat,
                            longitude: this.nowWeather.coord.lon,
                            latitudeDelta: 0.09,
                            longitudeDelta: 0.09}}
                            style={{...StyleSheet.absoluteFillObject}}
                            />
                    </View>
                </View>
            )
            }
    }
}
/*
<MapView initialRegion={{
                            latitude: this.nowWeather.coord.lat,
                            longitude: this.nowWeather.coord.lon,
                            latitudeDelta: 0.09,
                            longitudeDelta: 0.09
                        }}
                        style={{...StyleSheet.absoluteFillObject}}/>
*/



const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: "center",
        alignSelf: "stretch"
    },
    infoSection:{
        flex:1,
        alignSelf:'stretch'
    },
    iconAndBasceInfo:{
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#FFFC4A',
        flexDirection: 'row'
    },
    iconProper:{
        flex: 2.5,
        alignSelf: "stretch",
        alignItems: 'center',
        backgroundColor: '#F6D234'
    },
    basicInfo:{
        flex: 3,
        alignSelf: 'stretch',
        alignItems: 'center',
        backgroundColor: '#F5E147'
    },
    to5Day:{
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#FFFC4A'
    },
    tempInfo:{
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#BAE84F',
        justifyContent: 'center'
    },
    mapsection:{
        flex: 1,
        alignItems: "center",
        alignSelf: 'stretch',
        backgroundColor: 'black'
    },
    basicText: {
        fontSize: 20,
        textAlign: 'center'
    },
    textOutliner: {
        flex: 1,
        alignSelf: 'stretch',
        borderWidth: 2,
        borderColor: 'black'
    },
    windOutliner:{
        flex: 2,
        alignSelf: 'stretch',
        borderWidth: 2,
        borderColor: 'black'
    },
    temperatureText:{
        fontSize: 20,
        textAlign: 'center'
    },

})