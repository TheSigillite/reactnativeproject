import { Navigation } from "react-native-navigation";
import App from "./src/App";
import { registerScreens } from "./src/ScreensRegister";

registerScreens();

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            sideMenu: {
                left: {
                    component: {
                        id: 'SideStack',
                        name: 'PreviousLocations'
                    }
                },
                center: {
                    stack: {
                        id: 'MainStack',
                        children: [
                            {
                                component: {
                                    name: 'StartScreen'
                                },
                            },
                        ],
                        options: {
                            topBar: {
                                visible: false
                            }
                        }
                    }
                }
            }
        }
    });
});